<h1> My Books Table</h1>
<table>
    <tr>
        <th>
            id
        </th>
        <th>
            book title
        </th>
        <th>
            author name
        </th>
    </tr>

    @foreach($books as $book)
        <tr>
        <td>
            {{$book->id}}
            </td>
            <td>
            {{$book->title}}
            </td>
            <td>
            {{$book->author}}
            </td>
        </tr>
    @endforeach
</table>